TARGET = ../../web/nz2015.mini.debconf.org/

publish-static:
	cp -alf website/* $(TARGET)

build:	nz2015minidebconf.setup ikiwiki/*
	ikiwiki --setup nz2015minidebconf.setup 

publish:
	bin/release-to-server

stage:
	git add website ikiwiki cgi/ikiwiki.cgi nz2015minidebconf.setup

push: publish
	git push
	git push alioth

extract: debian-miniconf.csv

debian-miniconf.csv: bin/extract-programme website/Programme/index.html
	bin/extract-programme website/Programme/index.html > debian-miniconf.csv

.PHONY: publish stage push extract
